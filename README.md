# Assesment 2 : COMP6211 'Algorithms and Data Structures'
* This is for an assessment2 of __COMP6211 'Algorithms and Data Structures'__ in Toi-Ohomai.

## 1. Information
* **Student Name** : Shinkee Cho
* **Student ID**   : 10013041

## 2. Structure

>### Class Program (in Program.cs)
             |
             +--- Menu_Question1()  <= Q1
             |
	         +--- Menu_Question2()  <= Q2
	         |
	         +--- Menu_Question3()  <= Q3
	         |
	         +--- Menu_Question4()  <= Q4
	         |
	         |
	         +--- PrintDateTimeAtMMA()
	         |
	         +--- CompareBubbleSortsWithMoistureData()
	         |
	         +--- CompareBubbleSortsWithRandomData()


>### Class Moisture (in files: Question1.cs, Question2.cs, Question3.cs, Extra.cs)
             |
             +--- ImportData()     <= Q1
             |
	         +--- FindMaximum()    <= Q1
	         |
	         +--- PrintArray()     <= Q1
	         |
			 |
	         +--- SelectionSort()  <= Q2
	         |
	         +--- Max()           
	         |
	         +--- Min()           
	         |
	         +--- Average()
	         |
			 |
	         +--- LinearSearch()   <= Q3
			 |
	         +--- CompareArrays()  <= Q3
			 |
	         +--- GetDateTimes()
			 |
	         |
	         +--- ConvertStringToFloat()
	         |
	         +--- MakeRandomIntArray()


>### Class BubbleSortTiming (in Question4.BubbleSortTiming.cs)
             |
             +--- BubbleSort()           <= Q4
             |
	         +--- ImprovedBubbleSort()   <= Q4
	         |
	         +--- PrintElapsedTime()     <= Q4


## 3. What files is for the questions of this assessment? ###

>### 3-1. Question1

>>Requirement             |  File Name      | Line number | Note
>>:----------             |:-----------     |:-----------:|:----
>>__User Interface__      | Program.cs      |  176, 83    | Program.Menu_Question1(), Main()
>>__ImportData()__ Method | Question1.cs    |  18, 47     | Moisture.ImportData()
>>__FindMaximum()__ Method| Question1.cs    |  74         | Moisture.FindMaximum()
>>__PrintArray()__ Method | Question1.cs    |  131        | Moisture.PrintArray()

>### 3-2. Question2

>>Requirement                         |  File Name    | Line number | Note
>>:-----------------------------------|:--------------|:-----------:|:-----
>>__User Interface__                  | Program.cs    |  224, 83    | Program.Menu_Question2(), Main()
>>__SelectionSort()__ Method          | Question2.cs  |  17         | Moisture.SelectionSort()
>>__Unsorted array displayed__        | Program.cs    |  233        | Calls Moisture.PrintArray() in Program.Menu_Question2()
>>__Sorted array displayed__          | Program.cs    |  243        | Calls Moisture.PrintArray() in Program.Menu_Question2()
>>__Max, min and average displayed__  | Program.cs    |  255-257    | Calls Moisture.PrintArray() in Program.Menu_Question2()

>### 3-3. Question3

>>Requirement                                     |  File Name   | Line number  | Note
>>:---------------------------------------------- |:-------------|:------------:|:----
>>__User Interface__                              | Program.cs   |  266, 83     | Program.Menu_Question3(), Main()
>>__LinearSearch()__ Method                       | Question3.cs |  17          | Moisture.LinearSearch()
>>__CompareArrays()__ Method                      | Question3.cs |  60, 73      | Moisture.CompareArrays()
>>__Time/Date data copied to an array and used__  | Program.cs   |  269         | Calls Program.ImportData() in Program.Menu_Question3()
>>__Max, min and average occurrences displayed__  | Program.cs   |  289,296,303 | Calls Program.PrintDateTimeAtMMA() in Program.Menu_Question3()


>### 3-4. Question4 : Evaluating a Postfix Expression with a Stack

>>Requirement                           |  File Name                    | Line number | Note
>>:------------------------------------ |:------------------------------|:----------:|:----
>>__User Interface__                    | Program.cs                    | 312, 83    | Program.Menu_Question4(), Main()
>>__BubbleSort()__ Method               | Question4.BubbleSortTiming.cs | 27         | BubbleSortTiming.BubbleSort()
>>__ImprovedBubbleSort()__ Method       | Question4.BubbleSortTiming.cs | 81         | BubbleSortTiming.ImprovedBubbleSort()
>>__Improvement 1 implemented__         | Question4.BubbleSortTiming.cs | 105        | BubbleSortTiming.ImprovedBubbleSort()
>>__Improvement 2 implemented__         | Question4.BubbleSortTiming.cs | 97,122,131 | BubbleSortTiming.ImprovedBubbleSort()
>>__Timer implemented and displayed__   | Question4.BubbleSortTiming.cs | 34,69,89,138, 149 | BubbleSortTiming.BubbleSort(), BubbleSortTiming.ImprovedBubbleSort(), BubbleSortTiming.PrintElapsedTime()

>### 3-5. Extra Classes and Methods

>>Class / Method                     |  File Name  | Line number  | Note
>>:--------------------------------- |:----------- |:-----------: |:-----------
>>__ConvertStringToFloat()__ Method  | Extra.cs    |  14          | Moisture.ConvertStringToFloat()
>>__MakeRandomIntArray()__ Method    | Extra.cs    |  37          | Moisture.MakeRandomIntArray()




