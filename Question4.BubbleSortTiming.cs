using System;
using System.Diagnostics;

namespace SortingAndSearching
{
    /// <summary>
    /// This class has two method:BubbleSort() and ImprovedBubbleSort()
    /// to compare performances of the two methods.
    /// 
    /// In the two method, they all have generic type parameter,
    /// because the int type random numbers can be used in ths assessment.
    /// The Moisture Data given has only 100 data, then the time of sorting is too short.
    /// It is hard to compare two bubble sort method by only using the Moisture data given.
    /// </summary>
    public class BubbleSortTiming
    {
        /// <summary>
        /// Normal Bubble Sort
        ///
        /// The statement 'where T : System.IComparable<T>' at the end of method's name
        /// is using to compare two generic type elements in an array.
        /// The interface 'System.IComparable<T>' has a method CompareTo().
        /// </summary>
        /// <param name="arr">Source array to sort</param>
        /// <typeparam name="T">Gerneric type. In this program, int and float type are using.</typeparam>
        /// <returns>Sorted array</returns>
        public static T[] BubbleSort<T>(T[] arr) where T : System.IComparable<T>
        {
            T temp;
            T[] arrClone = (T[])arr.Clone();    // Copys the arr[] to prevent to modify.
            int arrLength = arrClone.Length;    // For better performance

            // Starts stopwatch.    
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // This for-loop makes that each element at index 'i' can be compared.
            // It is not efficient, but it is easy to understand.
            for (int i = 0; i < arrLength-1; i++)
            {
                // This for-loop iterates from 0 to the sorting array length - 1.
                // The sorted element will be bubbled by being swapped from the end of the array.
                for (int j = 0; j < arrLength-1; j++)
                {
                    // This method has generic type paramether.
                    // That means comparison is not going to work.
                    // So, CompareTo() method is using.
                    // 
                    // The if-statement using is the same as
                    // following if-statement if you do not use generic types.
                    // None gerneric type : if(arrClone[j] > arrClone[j+1])
                    if (arrClone[j].CompareTo(arrClone[j+1]) > 0)
                    {
                        // Swap them
                        temp = arrClone[j];
                        arrClone[j] = arrClone[j + 1];
                        arrClone[j + 1] = temp;
                    }
                }
            }

            // Ends stopwatch and prints the time
            stopWatch.Stop();
            PrintElapsedTime("Bubble Sort", ref stopWatch);

            return arrClone;
        }

        /// <summary>
        /// Improved Bubble Sort.
        /// This method has two improved statement compared to Normal Buble Sort.
        /// One is about the range of for-loop to sort,
        /// another is about checking if there is a swap.
        /// 
        /// The statement 'where T : System.IComparable<T>' at the end of method's name
        /// is using to compare two generic type elements in an array.
        /// The interface 'System.IComparable<T>' has a method CompareTo().
        /// </summary>
        /// <param name="arrClone"></param>
        /// <returns>Sorted array</returns>
        public static T[] ImprovedBubbleSort<T>(T[] arr) where T:System.IComparable<T>
        {
            T temp;
            bool swapped = false;
            T[] arrClone = (T[])arr.Clone();    // Copys the arr[] to prevent to modify.
            int arrLength = arrClone.Length;    // For better performance

            // Starts stopwatch.
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // This for-loop will be stopped before the value of variable 'write' becomes arrLength - 1.
            // Because the array will be sorted during iteration of this for-loop, then it makes break for this loop.
            for(int write = 0; write < arrLength - 1; write++)
            {
                // Initialise
                swapped = false;    // << [Improvement 2 implemented]

                // [Improvement 1 implemented]
                // ----------------------------------------------------------------------------
                // Sorted valued does not have to be sorted again.
                // The sorted value swaps and appends to the end of an array.
                // Therefore, the range to sort will be modified from 0 to arrLength-1-write
                // ----------------------------------------------------------------------------
                for(int bubble = 0; bubble < arrLength - 1 - write; bubble++)
                {
                    // This method has generic type paramether.
                    // That means comparison is not going to work.
                    // So, CompareTo() method is using.
                    // 
                    // The if-statement using is the same as
                    // following if-statement if you do not use generic types.
                    // None gerneric type : if(arrClone[bubble] > arrClone[bubble+1])
                    if(arrClone[bubble].CompareTo(arrClone[bubble+1]) > 0)  
                    {
                        // (1) swap process
                        temp = arrClone[bubble];
                        arrClone[bubble] = arrClone[bubble+1];
                        arrClone[bubble+1] = temp;

                        // Remember something changed
                        swapped = true;  // << [Improvement 2 implemented]
                    }
                }

                // [Improvement 2 implemented]
                // ----------------------------------------------------------------------------
                // Decides if it is sorted by checking whether any swaps were happened in (1) swap process.
                // If there is no swap, it is assumed that all values are sorted.
                // ----------------------------------------------------------------------------
                if(swapped == false)
                {
                    break;
                }
            }
            
            // Ends stopwatch and prints the time
            stopWatch.Stop();
            PrintElapsedTime("Improved Bubble Sort", ref stopWatch);

            return arrClone;
        }

        /// <summary>
        /// Prints the elapsed time of sorting algorithm.
        /// </summary>
        /// <param name="sortingAlgorithmName">Sorting Algorithm's Name to print</param>
        /// <param name="sw">Intance of class Stopwatch()</param>
        private static void PrintElapsedTime(string sortingAlgorithmName, ref Stopwatch sw)
        {
            TimeSpan ts;
            string elapsedTime;

            // Makes a string with elaped time.
            ts = sw.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            
            // Format and display the TimeSpan value.
            Console.WriteLine();
            Console.WriteLine("-------------------------------");
            Console.WriteLine(" " + sortingAlgorithmName);
            Console.WriteLine("-------------------------------");
            Console.WriteLine(" RunTime: " + elapsedTime);
            Console.WriteLine("-------------------------------");
        }
    }
}