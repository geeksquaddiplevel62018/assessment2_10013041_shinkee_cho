using System;
using System.IO;

namespace SortingAndSearching
{
    /// <summary>
    /// This Class implements methods required by question 1, 2 and 3 of assessment 2.
    /// In this file, all methods are ones for question 1.
    /// </summary>
    static partial class Moisture
    {
        /// <summary>
        /// Imports data from a file escpecially Moisture and Date time data.
        /// </summary>
        /// <param name="filepath">File path to import</param>
        /// <param name="data">String type array</param>
        /// <returns>Whether the importing is succeeded</returns>
        public static bool ImportData(string filepath, out string[] data)
        {
            // Print an error message and return false if it does not exist.
            if (!File.Exists(filepath))
            {
                Console.WriteLine();
                Console.WriteLine(" > [Error]");
                Console.WriteLine(" > Sorry, there is not the file '{0}'.", filepath);
                Console.WriteLine(" > If you are using Visual Studio instead of Visual Code,");
                Console.WriteLine(" > copy of the file from {0} to ./bin/debug or ./bin/release.", filepath);
                Console.WriteLine();
                data = null;
                return false;
            }

            // Read all data in the file as a string array.
            data = System.IO.File.ReadAllLines(filepath);
            
            return true;
        }

        /// <summary>
        /// Overloading method of ImportData().
        /// Because the moisture data shoud be assigned
        /// to float type array to compare the values of data.
        /// </summary>
        /// <param name="filepath">The path of file to import</param>
        /// <param name="data">The array to </param>
        /// <returns>Whether the importing is succeeded</returns>
        public static bool ImportData(string filepath, out float[] data)
        {
            string[] strData = null;

            // Imports the data of a file at the filepath,
            // and saves the data in the file by each line to a string array variable.
            // The string array is converted to a float type array.
            if(ImportData(filepath, out strData))
            {
                data = ConvertStringToFloat(strData);  // Converts string to float in array.
                return true;
            }
            // If fail, assigns null to fData and returns false.
            else
            {
                data = null;
                return false;
            }

        }

        /// <summary>
        /// Finds the largest numbers as many as required in moisture data.
        /// </summary>
        /// <param name="moistureData">Float type array of Moisture data</param>
        /// <param name="n">The quantity of the largest numbers to find</param>
        /// <returns>Requested Largest numbers</returns>
        public static float[] FindMaximum(float[] moistureData, int n)
        {
            float[] moistureDataClone = (float[])moistureData.Clone();  // Clones the original data not to be modified
            int cnt = moistureDataClone.Length;

            // If the quantity of the largest numbers to find is more than the quatity of moisture data,
            // limits the quantity of the largest numbers to the quantity of oisture data.
            if(n > cnt) { n = cnt; }
            float[] maxNumbers = new float[n];  // An array to assign largest numbers and to return.

            for(int i=0; i<n; i++)
            {
                // Assume the max is the first element.
                // This variable is an index of the array moistureDataClone
                // and it presents that the maxIndex-th value of the array is maximum number.
                int maxIndex = 0;

                // Variable j is assigned number '1'.
                // Because the maximum will be the first(0th) value in the array moistureDataClone[] 
                // and the maximum will be compared to 1th value in the array moistureDataClone[].
                for(int j=1; j<cnt; j++)
                {
                    // Compares current maximum value to jth value in the array moistureDataClone[].
                    // If the jth value is larger than the maximum value,
                    // assgins j to maxIndex variable.
                    if(moistureDataClone[maxIndex] < moistureDataClone[j]) 
                    {
                        maxIndex = j;
                    }
                }

                // Assigns the maximum value to array maxNumbers[].
                maxNumbers[i] = moistureDataClone[maxIndex];
                
                // Clears the values of the array which are the same as maximum value found
                // not to find the same value again.
                float tempMax = maxNumbers[i];  // Assigns maxium value found to a new variable just because of effiency
                for(int k=0; k<cnt; k++)
                {
                    // Sets zero at the position of array moistureDataClone[] of containing the same value as maximum value found.
                    if(moistureDataClone[k] == tempMax)
                    {
                        moistureDataClone[k] = 0;
                    }
                }
                
            }
            
            return maxNumbers;
        }

        /// <summary>
        /// (Overloading method) Prints out the data of an array on console screen.
        /// The type of the array is decided by Generic symbol <T>.
        /// </summary>
        /// <param name="arr">Array to print the values</param>
        /// <typeparam name="T">Generic type of the input array.</typeparam>
        public static void PrintArray<T>(T[] arr)
        {
            PrintArray<T>("", arr, " ", 10);
        }

        /// <summary>
        /// Prints out the data of an array on console screen.
        /// The type of the array is decided by Generic symbol <T>.
        /// </summary>
        /// <param name="prefix">Value to prepend to the front of value of arr</param>
        /// <param name="arr">Array to print</param>
        /// <param name="postfix">Value to append to the end of value of arr</param>
        /// <param name="column">Quantity of values to show in a line</param>
        /// <typeparam name="T">Generic type</typeparam>
        public static void PrintArray<T>(object prefix, T[] arr, object postfix, int column)
        {
            string value = string.Empty;    // Initialises

            // Prints values of the array with prefix and postfix.
            for(int i=0; i<arr.Length; i++)
            {
                // Begins a new line after printing as many as number of column.
                if(i % column == 0){ Console.WriteLine(); }

                // If the type of the array arr[] is float type,
                // defines the width of the string field as a 5-character string,
                // and displays two zero integral digits. 
                if(typeof(T) == typeof(float)){ value = string.Format("{0,5:0.00}",arr[i]); }
                // Otherwise, just assgins the value of array arr[] to variable 'value'.
                else { value = arr[i].ToString(); }
                
                // Prints prifix, value of arr[] and postfix.
                Console.Write("{0} {1,6} {2}",prefix.ToString(), value, postfix.ToString());
            }
        }
    }
}
