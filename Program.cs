﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SortingAndSearching
{
    /// <summary>
    /// This class has a structure to store information for Max, Min, Average of Moisture data given.
    /// </summary>
    public class MaxMinAverage
    {
        /// <summary>
        /// Contains values, indices and Date&Time of max, min and average of Moisture data.
        /// </summary>
        public struct Info
        {
            /// <summary>
            /// Value of max, min and average of Moisture data.
            /// The initial value is null.
            /// </summary>
            public float? Value;

            /// <summary>
            /// List of indices which are indexing the same value of max, min, average in Moisture data.
            /// </summary>
            public  List<int> Indices;

            /// <summary>
            /// Key of dictionary is index number, and value of dictionary is datetime in regard of Moisture data.
            /// </summary>
            public Dictionary<int, string> DateTimes;

            /// <summary>
            /// Constructor of Info struct.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="indices"></param>
            /// <param name="dateTimes"></param>
            public Info(float? value, List<int> indices, Dictionary<int, string> dateTimes)
            {
                Value = value;
                Indices = indices;
                DateTimes = dateTimes;
            }
        }

        /// <summary>
        /// This field contains Information(value, indices and dateTime) about Maximum of Moisture Data.
        /// </summary>
        public Info Max;

        /// <summary>
        /// This field contains Information(value, indices and dateTime) about Minimum of Moisture Data.
        /// </summary>
        public Info Min;
        
        /// <summary>
        /// /// <summary>
        /// This field contains Information(value, indices and dateTime) about Average of Moisture Data.
        /// </summary>
        /// </summary>
        public Info Average;

        /// <summary>
        /// Constructor
        /// </summary>
        public MaxMinAverage()
        {
            // Initializes fields of Info struct and creates instance of Info struct.
            Max = new Info(null, new List<int>(), new Dictionary<int, string>());
            Min = new Info(null, new List<int>(), new Dictionary<int, string>());
            Average = new Info(null, new List<int>(), new Dictionary<int, string>());
        }
    }



    class Program
    {
        private const string FILEPATH_MOISTURE_DATA = "./Moisture_Data.txt";  // File Path of Moisture Data
        private const string FILEPATH_DATETIME_DATA = "./DateTime_Data.txt";  // File Path of Date Time Data

        static void Main(string[] args)
        {
            ConsoleKey inputKey;                // To contain key value which user typed in to select a menu.
            float[] moistureRawData = null;     // To contain original Moisture data.
            MaxMinAverage mma = new MaxMinAverage();    // Instance of MaxMinAverage to save information
                                                        // of maximum, minimum and average of Moisture and Date&Time data.
            
            // Import Moisture data from given Moisture_Data.txt file.
            bool imported = Moisture.ImportData(FILEPATH_MOISTURE_DATA, out moistureRawData);
            
            // Check if imported
            if(!imported)
            {
                Console.WriteLine(" > Sorry! Please, check the error message above and run it again.");
                Console.WriteLine();
                Console.ReadLine();
                return;
            }

            do
            {
                // Shows main menu.
                Console.Clear();
                Console.WriteLine("------------------------------------------------------------------------");
                Console.WriteLine();
                Console.WriteLine(" 1. (Q1) Find the largest Numbers");
                Console.WriteLine();
                Console.WriteLine(" 2. (Q2) Sort by selection sort algorithm");
                Console.WriteLine("         (Output: the original state of the array, the final sorted array,");
                Console.WriteLine("                  the maximum, minimum and average of the values in the array)");
                Console.WriteLine();
                Console.WriteLine(" 3. (Q3) Print dates and times of the max, min and average value");
                Console.WriteLine();
                Console.WriteLine(" 4. (Q4) Compare bubble sort algorithms");
                Console.WriteLine("         (Output: The resulting times of both bubble sort");
                Console.WriteLine();
                Console.WriteLine(" 0. Exit");
                Console.WriteLine();
                Console.WriteLine("------------------------------------------------------------------------");

                // Selects a menu.
                Console.WriteLine();
                Console.Write(" > Type in one of above numbers : ");
                inputKey = Console.ReadKey().Key;
                Console.WriteLine();

                // Checks the value of input key and calls an appropriate method for each menu.
                switch(inputKey)
                {
                    // Menu of Question 1
                    case ConsoleKey.D1 :
                        Menu_Question1(moistureRawData);
                        break;

                    // Menu of Question 2
                    case ConsoleKey.D2 :
                        Menu_Question2(moistureRawData, ref mma);
                        break;

                    // Menu of Question 3
                    case ConsoleKey.D3 :
                        Menu_Question3(moistureRawData, ref mma);
                        break;

                    // Menu of Question 4
                    case ConsoleKey.D4 :
                        Menu_Question4(moistureRawData);
                        break;

                    // Quits this application.
                    case ConsoleKey.D0 :
                        Console.WriteLine();
                        Console.WriteLine(" > Thank you!");
                        break;

                    // If user input wrong key...
                    default :
                        Console.WriteLine();
                        Console.WriteLine(" > !!! Please, type in a correct number.");
                        break;
                }

                // Stops to show the result of each menu till pressing any key.
                Console.Write("\n\n > Press any keys! ");
                Console.ReadLine();

            } while(inputKey != ConsoleKey.D0); // If number '0' is input, quit otherwise continue the do-while loop.
        }

        /// <summary>
        /// Displays interface and result of Question 1.
        /// </summary>
        /// <param name="moistureData">Float type array of Moisture data</param>
        private static void Menu_Question1(float[] moistureData)
        {
            int nLargest = 0;
            
            // Gets quantity of largest numbers to find.
            Console.WriteLine();
            Console.Write(" > How many largest numbers do you want to find? : ");
            Int32.TryParse(Console.ReadLine(), out nLargest);

            // Prints out imported moisture data.
            Console.WriteLine();
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Imported moisture data");
            Console.WriteLine(" ----------------------------------- ");
            Moisture.PrintArray<float>(moistureData);
    
            // Finds the largest numbers.
            // * FindMaximum() method limits the quantity of largest numbers to the quantity of moisture data.
            float[] largestNumbers = Moisture.FindMaximum(moistureData, nLargest);

            // Prints out the largest numbers found.
            Console.WriteLine("\n");
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> {0} largest numbers", largestNumbers.Length);
            Console.WriteLine(" ----------------------------------- ");
            Moisture.PrintArray<float>(largestNumbers);
            Console.WriteLine();

            if(nLargest > 89)
            {
                Console.WriteLine();
                Console.WriteLine(" !!!: The zeros are duplicated numbers. ");
            }
            
            // If the quantity of largest numbers more than the quantity of moisture data,
            // prints out a message.
            if(nLargest > moistureData.Length)
            {
                Console.WriteLine();
                Console.WriteLine(" !!!: The number '{0}' you input is over than the number '{1}' of Moisture data.", nLargest, moistureData.Length);
            }
        }

        /// <summary>
        /// Menu of Question 2.
        /// </summary>
        /// <param name="moistureData">Array of Moisture data</param>
        /// <param name="mma">A struct containing Max, Min and Average of Moisture data</param>
        private static void Menu_Question2(float[] moistureData, ref MaxMinAverage mma)
        {
            float[] unsortedMoistureData = (float[])moistureData.Clone();   // Clones the original data not to be modified
            float[] sortedMoistureData = null;  // To assign sorted moisture data.

            // Prints the original state of Moisture data
            Console.WriteLine();
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Unsorted data");
            Console.WriteLine(" ----------------------------------- ");
            Moisture.PrintArray<float>(moistureData);
            

            // Sorts the Moisture data in ascending order using Selection sort algorithm,
            // and prints the sorted data.
            Console.WriteLine("\n");
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Sorted data by Selection Sort");
            Console.WriteLine(" ----------------------------------- ");
            sortedMoistureData = Moisture.SelectionSort(unsortedMoistureData);
            Moisture.PrintArray<float>(sortedMoistureData);

            // Get the values of max, min and average of moisture data.
            mma.Min.Value = sortedMoistureData[0];
            mma.Max.Value = sortedMoistureData[sortedMoistureData.Length-1];
            mma.Average.Value = Moisture.Average(sortedMoistureData);
            
            // Display values of max, min and averages.
            Console.WriteLine("\n");
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Max, Min and Average");
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" Max    : {0}", mma.Max.Value);
            Console.WriteLine(" Min    : {0}", mma.Min.Value);
            Console.WriteLine(" Average: {0}", mma.Average.Value);
        }

        /// <summary>
        /// Menu of Question 3.
        /// </summary>
        /// <param name="moistureData">Array of Moisture data</param>
        /// <param name="mma"></param>
        private static void Menu_Question3(float[] moistureData, ref MaxMinAverage mma)
        {
            // Import Date&Time data from Datetime_Data.txt file given.
            string[] dateTimeRawData;
            bool imported = Moisture.ImportData(FILEPATH_DATETIME_DATA, out dateTimeRawData);

            // Check if imported
            if(!imported)
            {
                Console.WriteLine(" > Sorry! Please, check the error message above and run it again.");
                Console.WriteLine();
                Console.ReadLine();
                return;
            }

            // Finds the values of maxs, mins and averages of moisture data,
            // and Compares and figures out the date & time related to moisture data.
            Moisture.LinearSearch(moistureData, ref mma);
            Moisture.CompareArrays(dateTimeRawData, ref mma);

            // Prints out the date & time of maximum values.
            Console.WriteLine();
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Maximum value : {0}", mma.Max.Value);
            Console.WriteLine(" ----------------------------------- ");
            PrintDateTimeAtMMA(mma.Max);

            // Prints out the date & time of minimum values.
            Console.WriteLine();
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Min value : {0}", mma.Min.Value);
            Console.WriteLine(" ----------------------------------- ");
            PrintDateTimeAtMMA(mma.Min);

            // Prints out the date & time of average values.
            Console.WriteLine();
            Console.WriteLine(" ----------------------------------- ");
            Console.WriteLine(" >> Average value : {0}", mma.Average.Value);
            Console.WriteLine(" ----------------------------------- ");
            PrintDateTimeAtMMA(mma.Average);
        }

        /// <summary>
        /// Menu of Question 4.
        /// </summary>
        /// <param name="moistureData">Array of Moisture data</param>
        private static void Menu_Question4(float[] moistureData)
        {
            ConsoleKey inputKey;

            do
            {
                // Sub Menu
                Console.Clear();
                Console.WriteLine(" ------------------------------------ ");
                Console.WriteLine("  Compare two different Bubble Sorts");
                Console.WriteLine(" ------------------------------------ ");
                Console.WriteLine("  1. By using Moisture Data");
                Console.WriteLine("  2. By using Random Numbers");
                Console.WriteLine();
                Console.WriteLine("  3. By using Moisture Data and print data");
                Console.WriteLine("  4. By using Random Numbers and print data");
                Console.WriteLine();
                Console.WriteLine("  0. Main Menu");
                Console.WriteLine(" ----------------------------------- ");
                Console.WriteLine();
                Console.Write(" > Choose one of the above numbers : ");

                inputKey = Console.ReadKey().Key;

                Console.WriteLine();

                switch(inputKey)
                {
                    // Number 1 : Bubble sort with Moisture data.
                    case ConsoleKey.D1:
                        CompareBubbleSortsWithMoistureData(moistureData, false);
                        break;

                    // Number 2 : Buble sort with Random numbers.
                    case ConsoleKey.D2:
                        CompareBubbleSortsWithRandomData(moistureData, false);
                        break;
                    
                    // Number 3 : Bubble sort with Moisture data and Prints data.
                    case ConsoleKey.D3:
                        CompareBubbleSortsWithMoistureData(moistureData, true);
                        break;

                    // Number 4 : Buble sort with Random numbers and Prints data.
                    case ConsoleKey.D4:
                        CompareBubbleSortsWithRandomData(moistureData, true);
                        break;

                    // Number 0 : Go back to main menu.
                    case ConsoleKey.D0:
                        return;     // To ignore to call Console.ReadKey() out of this switch statement.

                    // If a worng number is input...
                    default:
                        Console.WriteLine();
                        Console.WriteLine(" > !!! Please, type in a correct number.");
                        break; 
                }

                Console.Write("\n\n > Press any keys! ");
                Console.ReadKey();

            }while(inputKey != ConsoleKey.D0);  // If not '0', continues this do-while loop.
        }

        /// <summary>
        /// Prints date&time in regard to max, min and average found out.
        /// </summary>
        /// <param name="info">Reference of MaxMinAverage.Info struct containing information of Max, Min and Average</param>
        private static void PrintDateTimeAtMMA(MaxMinAverage.Info info)
        {
            Console.WriteLine("   Index        Date&Time");

            for(int i=0; i < info.Indices.Count;i++)
            {
                Console.WriteLine("    [{0,2:0}]    {1}", info.Indices[i], info.DateTimes[info.Indices[i]]);
            }
        }

        /// <summary>
        /// Calls BubbleSort() and ImprovedBubbleSort with moisture data to compare the performanc.
        /// (Optional) Prints data such as unsorted data and sorted data.
        /// </summary>
        /// <param name="moistureData">Original Moisture data</param>
        /// <param name="doesPrint">Decides if print data</param>
        private static void CompareBubbleSortsWithMoistureData(float[] moistureData, bool doesPrint)
        {
            // Just sorts data
            if(doesPrint == false)
            {
                BubbleSortTiming.BubbleSort(moistureData);
                BubbleSortTiming.ImprovedBubbleSort(moistureData);
            }
            // Sorts and Prints data
            else
            {
                // Prints unsorted data.
                Console.WriteLine();
                Console.WriteLine("-------------------------------");
                Console.WriteLine(" Unsorted data");
                Console.WriteLine("-------------------------------");
                Moisture.PrintArray(moistureData);
                Console.WriteLine();

                float[] sortedData;

                // Sorts and prints sorted data by BubbleSort().
                sortedData = BubbleSortTiming.BubbleSort(moistureData);
                Moisture.PrintArray(sortedData);
                Console.WriteLine();

                // Sorts and prints sorted data by ImprovedBubbleSort().
                sortedData = BubbleSortTiming.ImprovedBubbleSort(moistureData);
                Moisture.PrintArray(sortedData);
            }
            
        }

        /// <summary>
        /// Calls BubbleSort() and ImprovedBubbleSort with moisture data to compare the performanc
        /// by using random numbers instead of moisture data.
        /// (Optional) Prints data such as unsorted data and sorted data.
        /// </summary>
        /// <param name="moistureData">Original Moisture data</param>
        /// <param name="doesPrint">Decides if print data</param>
        private static void CompareBubbleSortsWithRandomData(float[] moistureData, bool doesPrint)
        {
            int cnt;    // Quantity of random numbers.
                        
            Console.WriteLine();
            Console.Write(" > How many random numbers do you want to create? : ");
            
            // If the input is not number, error message will be printed.
            if(!Int32.TryParse(Console.ReadLine(), out cnt))
            {
                Console.Write(" > !!! Please, type in only a number.");
                return;
            }
            
            // If the quantity of random numbers is larger than 1000,
            // ask if display.
            if(doesPrint && (cnt > 1000))
            {
                Console.Write(" > Do you really want to display the {0} numbers? ('y' or 'n') : ", cnt);
                ConsoleKey inputKey = Console.ReadKey().Key;

                // If not display, go back to menu.
                if(inputKey != ConsoleKey.Y){ return; }
            }

            // Makes random numbers as many as input
            // and return an int type array containing the numbers.
            int[] arrRandom = Moisture.MakeRandomIntArray(cnt, 0, cnt*10);

            // Print mode : prints unsorted data.
            if(doesPrint)
            {
                Console.WriteLine();
                Console.WriteLine("-------------------------------");
                Console.WriteLine(" Unsorted data");
                Console.WriteLine("-------------------------------");
                Moisture.PrintArray(arrRandom);
                Console.WriteLine();
            }

            // Sorts with a normal Bubble sort.
            int[] arrRandomClone = (int[])arrRandom.Clone();    // Copy of the random number array not to be modified the original source.
            int[] sortedData = BubbleSortTiming.BubbleSort(arrRandomClone);
            
            // Print mode : prints sorted data by BubbleSort().
            if(doesPrint)
            {
                Moisture.PrintArray(sortedData);
                Console.WriteLine();
            }
            
            // Sorts with an Improved Bubble sort.
            arrRandomClone = (int[])arrRandom.Clone();          // Copy of the random number array to test with the same source.
            sortedData = BubbleSortTiming.ImprovedBubbleSort(arrRandomClone);

            // Print mode : prints sorted data by ImproveedBubbleSort().
            if(doesPrint)
            {
                Moisture.PrintArray(sortedData);
            }           
        }
    }
}
