using System;
using System.Collections.Generic;

namespace SortingAndSearching
{
    /// <summary>
    /// This Class implements methods required by question 1, 2 and 3 of assessment 2.
    /// In this file, all methods are ones for question 2.
    /// </summary>
    static partial class Moisture
    {
        /// <summary>
        /// Selection Sort Algorithm.
        /// </summary>
        /// <param name="unsortedArray">An array to sort</param>
        /// <returns>Sorted array</returns>
        public static float[] SelectionSort(float[] unsortedArray)
        {
            int minIndex;
            int arrLength = unsortedArray.Length;                   // Length of array arrSource[].
            float[] sortedArray = (float[])unsortedArray.Clone();   // Clone the original data not to be modified
            float temp;

            // Advance the position through the entire array.
            // * could do 'i < arrLength - 1' because single element is also min element
            for(int i=0; i<arrLength-1; i++)
            {
                // Assume the min is the first element
                minIndex = i;

                // Test against elements after i to find the smallest
                for(int j=i+1; j<arrLength; j++)
                {
                    // If this element is less, then it is the new minimum
                    if(sortedArray[j] < sortedArray[minIndex])
                    {
                        // Assign index of minimum value.
                        minIndex = j;
                    }
                }

                // Swap the i-th value for new minium value in array sortedArray.
                temp = sortedArray[i];
                sortedArray[i] = sortedArray[minIndex];
                sortedArray[minIndex] = temp;
            }
            
            return sortedArray;
        }


        /// <summary>
        /// Finds maximum number in the first parameter array arr[].
        /// !!! : This method is used when the max is required without sorting.
        /// </summary>
        /// <param name="arr">Array to be found max from</param>
        /// <returns>Max value</returns>
        public static float Max(float[] arr)
        {
            float max = 0;                  // Assume the max is the first element.

            foreach(float f in arr)         // Search all values in array arr[].
            {
                if(f > max){ max = f; }     // If this element is larger, then it is the new maximum.
            }

            return max;
        }

        /// <summary>
        /// Finds minimum number in the first parameter array arr[].
        /// !!! : This method is used when the min is required without sorting.
        /// </summary>
        /// <param name="arr">Array to be found min from</param>
        /// <returns>Min value</returns>
        public static float Min(float[] arr)
        {
            float min = arr[0];             // Assume the min is the first element.

            foreach(float f in arr)         // Search all values in array arr[].
            {
                if(f < min){ min = f; }     // If this element is less, then it is the new minimum.
            }

            return min;
        }


        /// <summary>
        /// Finds average number in the first parameter array arr[].
        /// </summary>
        /// <param name="arr">Array to be found average from</param>
        /// <returns>Average value</returns>
        public static float Average(float[] arr)
        {
            float sum = 0;                                  // Initialise

            foreach(float f in arr) { sum += f; }           // Sum all values of array arr[].

            return (float)Math.Round(sum/arr.Length, 2);    // Return the average. (Rounds the average to two fractional digits)
        }
    }
}
