using System;
using System.IO;

namespace SortingAndSearching
{
    static partial class Moisture
    {
        /// <summary>
        /// Convers string type array to float type array.
        /// This method will be used for Moisture data to convert.
        /// </summary>
        /// <param name="arrStr"></param>
        /// <returns></returns>
        public static float[] ConvertStringToFloat(string[] arrStr)
        {
            float[] arrFloat = new float[arrStr.Length];

            for(int i=0; i<arrStr.Length; i++)
            {
                float.TryParse(arrStr[i], out arrFloat[i]); // Converts
            }

            return arrFloat;
        }

        /// <summary>
        /// Makes a random int-type array.
        /// 
        /// In question 4, the comparison of two bubble sort method with Moisture data given
        /// is finished too fast.
        /// This method helps the comparison by making an array with large numbers.
        /// </summary>
        /// <param name="cnt">Quantity of random numbers to be made</param>
        /// <param name="min">Minimum number in the range of random numbers</param>
        /// <param name="max">Maximum number in the range of random numbers</param>
        /// <returns></returns>
        public static int[] MakeRandomIntArray(int cnt, int min = 0, int max = 100)
        {
            int[] arr = new int[cnt];   // Array to contain random numbers.
            Random r = new Random();    // Creates an instance of class Random().

            for(int i=0; i<cnt; i++)
            {
                arr[i] = r.Next(min, max);  // Makes a random number.
            }

            return arr;
        }
    }
}