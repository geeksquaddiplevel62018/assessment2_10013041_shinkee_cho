using System;
using System.Collections.Generic;

namespace SortingAndSearching
{
    /// <summary>
    /// This Class implements methods required by question 1, 2 and 3 of assessment 2.
    /// In this file, all methods are ones for question 3.
    /// </summary>
    public static partial class Moisture
    {
        /// <summary>
        /// Finds the indices at which max, min and average values occurred. 
        /// </summary>
        /// <param name="moistureData">Original Moisture data</param>
        /// <param name="mma">A struct contained information for max, min and average</param>
        public static void LinearSearch(float[] moistureData, ref MaxMinAverage mma)
        {
            int cnt = moistureData.Length;

            // Check if max, min and average are set.
            // If not, find the max, min an average by calling appropriate method.
            // * mma.Max.Value, mma.Min.Value and mma.Average.value are assigned 'null' as default value.
            float? max = mma.Max.Value = mma.Max.Value ?? Max(moistureData);
            float? min = mma.Min.Value = mma.Min.Value ?? Min(moistureData);
            float? average = mma.Average.Value = mma.Average.Value ?? Average(moistureData);

            for(int i = 0; i<cnt;i++)
            {
                if(moistureData[i] == max)              // If the element is equal to max...
                {
                    if(!mma.Max.Indices.Contains(i))    // If not contained index i yet...
                    {                                   // (This method can be called again, that means it will be duplicated)
                        mma.Max.Indices.Add(i);         // Add the index i.
                    }
                }
                else if(moistureData[i] == min)         // If the element is equal to min...
                {
                    if(!mma.Min.Indices.Contains(i))    // If not contained index i yet...
                    {                                   // (This method can be called again, that means it will be duplicated)
                        mma.Min.Indices.Add(i);         // Add the index i.
                    }
                }
                else if(moistureData[i] == average)      // If the element is equal to average...
                {
                    if(!mma.Average.Indices.Contains(i)) // If not contained index i yet...
                    {                                    // (This method can be called again, that means it will be duplicated)
                        mma.Average.Indices.Add(i);      // Add the index i.
                    }
                }
            }
        }

        /// <summary>
        /// Compares array dateTimeData[] to arrays such as max, min and average found out,
        /// then finds date&time data in array dateTimeData[] with indices at the max, min adn average values occured.
        /// </summary>
        /// <param name="dateTimeData">Original Date&Time data related to Moisture data</param>
        /// <param name="mma">An instance of MaxMinAverage class contained information for max, min and average</param>
        public static void CompareArrays(string[] dateTimeData, ref MaxMinAverage mma)
        {
            mma.Max.DateTimes = GetDateTimes(dateTimeData, ref mma.Max);
            mma.Min.DateTimes = GetDateTimes(dateTimeData, ref mma.Min);
            mma.Average.DateTimes = GetDateTimes(dateTimeData, ref mma.Average);
        }

        /// <summary>
        /// Gets and returns date&time in array dateTimeData[] with indices at the max, min adn average values occured.
        /// </summary>
        /// <param name="dateTimeData">Original Date&Time data related to Moisture data</param>
        /// <param name="target">A struct MaxMinAverage.Info contained information for max, min and average</param>
        /// <returns></returns>
        private static Dictionary<int, string> GetDateTimes(string[] dateTimeData, ref MaxMinAverage.Info target)
        {
            // Search all indices contained in indices of max, min and average occured
            foreach(int index in target.Indices)
            {
                if(!target.DateTimes.ContainsKey(index))                // To prevent to add a duplicated index
                {                                                       // when this method is called more than two. 
                    target.DateTimes.Add(index, dateTimeData[index]);   // Adds index and date&time value.
                }
            }

            return target.DateTimes;
        }
    }
}
